const get = require("lodash/get");
const isUndefined = require("lodash/isUndefined");
const url = require("url");
const md5 = require("md5");

export default (req, res) => {
  const { body } = req;

  const { query, host, pathname } = url.parse(req.url, true);

  const dest = query.dest;
  const brackets = dest.match(/\[(.*?)\]/g);

  let endUrl = dest;
  let log = [];

  brackets.forEach(bracket => {
    const bracketContent = bracket.replace("[", "").replace("]", "");
    const [cmd, varName] = bracketContent.split(":");

    const rawValue = get(body, varName);

    if (isUndefined(rawValue)) throw new Error(`Could not find variable ${varName} from ${bracketContent}`);

    const value = rawValue;
    const md5Value = md5(rawValue);

    log.push({ bracketContent, cmd, varName, rawValue, md5Value });

    endUrl = endUrl
      .split(`[var:${varName}]`).join(value)
      .split(`[md5:${varName}]`).join(md5Value);
  });

  res.json({ query, host, pathname, body, log, endUrl });
};
